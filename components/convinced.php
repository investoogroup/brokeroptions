<section class="convinced">
  <div class="container">
    <div class="convinced__wrapper">
      <h2 class="convinced__header">
        Still not convinced?
      </h2>
      <h3 class="convinced__sub-header">Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse quos odit expedita sint voluptas fuga blanditiis saepe debitis officiis.</h3>
      <a href="#" class="convinced__button">Learn</a>
    </div>
  </div>
</section>