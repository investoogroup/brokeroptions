<section class="banner">
  <div class="container">
    <div class="banner__wrapper">
      <div class="banner__logo">LOGO</div>
      <a href="#" class="banner__cta">Trade Now</a>
      <ul class="banner__coins">
        <li class="banner__coin">
          <div class="banner__coin-name">BTC</div>
          <div class="banner__coin-price">$8,460</div>
          <div class="banner__coin-percent banner__coin-percent--plus">+1.76%</div>
        </li>
        <li class="banner__coin">
          <div class="banner__coin-name">BTC</div>
          <div class="banner__coin-price">$8,460</div>
          <div class="banner__coin-percent banner__coin-percent--plus">+1.76%</div>
        </li>
        <li class="banner__coin">
          <div class="banner__coin-name">BTC</div>
          <div class="banner__coin-price">$8,460</div>
          <div class="banner__coin-percent banner__coin-percent--minus">-1.76%</div>
        </li>
        <li class="banner__coin">
          <div class="banner__coin-name">BTC</div>
          <div class="banner__coin-price">$8,460</div>
          <div class="banner__coin-percent banner__coin-percent--plus">+1.76%</div>
        </li>
        <li class="banner__coin">
          <div class="banner__coin-name">BTC</div>
          <div class="banner__coin-price">$8,460</div>
          <div class="banner__coin-percent banner__coin-percent--plus">+1.76%</div>
        </li>
        <li class="banner__coin">
          <div class="banner__coin-name">BTC</div>
          <div class="banner__coin-price">$8,460</div>
          <div class="banner__coin-percent banner__coin-percent--plus">+1.76%</div>
        </li>
      </ul>
    </div>
  </div>
</section>