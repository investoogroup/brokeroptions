<div class="partner">
  <div class="partner__wrapper row">
    <div class="col-md-5">
      <img src="media/images/transferwise-logo.png" alt="" class="partner__logo">
      <div class="partner__info-container">
        <div class="partner__info">
          <h3 class="partner__name">TransferWise</h3>
          <div class="partner__stars">
            <img src="./media/images/yellow-star.png" alt="" class="partner__star">
            <img src="./media/images/yellow-star.png" alt="" class="partner__star">
            <img src="./media/images/yellow-star.png" alt="" class="partner__star">
            <img src="./media/images/yellow-star.png" alt="" class="partner__star">
            <img src="./media/images/yellow-star.png" alt="" class="partner__star">
          </div>
          <div class="partner__recommend">
            <span class="partner__recommend--highlighted">9/10</span>
            Users recommend this platform
          </div>
          <p class="partner__description">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Exercitationem quasi quis harum quod, earum maxime commodi nulla ipsa. Sequi iure adipisci necessitatibus vitae harum praesentium alias odio maiores asperiores laboriosam.</p>
          <a href="" class="partner__cta partner__cta--lg">Trade Now</a>
        </div>
      </div>
    </div>
    <div class="col-md-7 partner__details">
      <div class="partner__detail partner__detail--first">
        <h4 class="partner__detail-header">Key Features</h4>
        <ul class="partner__keyfeatures">
          <li class="partner__list-item">
            <img src="media/images/tick.png" class="partner__list-icon">
            <div class="partner__list-tag">This is an item</div>
          </li>
          <li class="partner__list-item">
            <img src="media/images/tick.png" class="partner__list-icon">
            <div class="partner__list-tag">This is an item</div>
          </li>
          <li class="partner__list-item">
            <img src="media/images/tick.png" class="partner__list-icon">
            <div class="partner__list-tag">This is an item</div>
          </li>
        </ul>
      </div>
      <div class="partner__toggle partner__toggle--inactive" data-toggle-text>
        <div class="partner__detail">
          <h4 class="partner__detail-header">Payment Methods</h4>
          <div class="row">
            <div class="col-6 col-sm-12">
              <div class="partner__paymentM">
                <div class="partner__list-item">
                  <img src="media/images/creditcard.png" class="partner__list-icon">
                  <div class="partner__list-tag">This is an item</div>
                </div>
                <div class="partner__list-item">
                  <img src="media/images/creditcard.png" class="partner__list-icon">
                  <div class="partner__list-tag">This is an item</div>
                </div>
                <div class="partner__list-item">
                  <img src="media/images/creditcard.png" class="partner__list-icon">
                  <div class="partner__list-tag">This is an item</div>
                </div>
              </div>
            </div>
            <div class="col-6 col-sm-12">
              <div class="partner__paymentM">
                <div class="partner__list-item">
                  <img src="media/images/creditcard.png" class="partner__list-icon">
                  <div class="partner__list-tag">This is an item</div>
                </div>
                <div class="partner__list-item">
                  <img src="media/images/creditcard.png" class="partner__list-icon">
                  <div class="partner__list-tag">This is an item</div>
                </div>
                <div class="partner__list-item">
                  <img src="media/images/creditcard.png" class="partner__list-icon">
                  <div class="partner__list-tag">This is an item</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="partner__details partner__details--small">
          <div class="partner__detail">
            <h4 class="partner__detail-header">Minimum Deposit</h4>
            <div class="partner__figure">£200</div>
          </div>
          <div class="partner__detail">
            <h4 class="partner__detail-header">Number of Assets</h4>
            <div class="partner__figure">£200</div>
          </div>
        </div>
        <div class="partner__detail partner__detail--last">
          <h4 class="partner__detail-header">Support</h4>
          <ul class="partner__support">
            <li class="partner__list-item">
              <img src="media/images/tick.png" class="partner__list-icon">
              <div class="partner__list-tag">This is an item</div>
            </li>
            <li class="partner__list-item">
              <img src="media/images/tick.png" class="partner__list-icon">
              <div class="partner__list-tag">This is an item</div>
            </li>
            <li class="partner__list-item">
              <img src="media/images/tick.png" class="partner__list-icon">
              <div class="partner__list-tag">This is an item</div>
            </li>
          </ul>
        </div>
      </div>
      <div class="partner__block-sm">
        <button data-toggle-button  class="partner__detail partner__detail--button">Show More</button>
        <a href="" class="partner__cta partner__cta--sm">Trade Now</a>
      </div>
    </div>
  </div>
</div>