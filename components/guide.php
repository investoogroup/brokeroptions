<section class="guide">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h2 class="guide__header">This section is about <span class="guide__header--highlighted">investing in Crypto</span></h2>
        <h3 class="guide__sub-header">What are Cryptocurrencies</h3>
        <h4 class="guide__sub-sub-header">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h4>
        <ul class="guide__list">
          <li class="guide__list-item">
            <div class="guide__list-bullet guide__list-bullet--colored">1</div>
            <div class="guide__list-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit.</div>
          </li>
          <li class="guide__list-item">
            <div class="guide__list-bullet guide__list-bullet--colored">1</div>
            <div class="guide__list-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit.</div>
          </li>
          <li class="guide__list-item">
            <div class="guide__list-bullet guide__list-bullet--colored">1</div>
            <div class="guide__list-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit.</div>
          </li>
          <li class="guide__list-item">
            <div class="guide__list-bullet guide__list-bullet--colored">1</div>
            <div class="guide__list-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit.</div>
          </li>
        </ul>
        <div class="guide__cta ">
          <h3 class="guide__sidebar-header">Ready to Invest?</h3>
          <a href="#" class="guide__btn">Start Trading</a>
        </div>
        <h4 class="guide__sub-sub-header">Lorem ipsum dolor sit amet consectetur adipisicing elit.</h4>
        <ul class="guide__list">
          <li class="guide__list-item">
            <div class="guide__list-bullet"><img src="./media/images/tick-solid.png"></div>
            <div class="guide__list-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit.</div>
          </li>
          <li class="guide__list-item">
            <div class="guide__list-bullet"><img src="./media/images/tick-solid.png"></div>
            <div class="guide__list-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit.</div>
          </li>
          <li class="guide__list-item">
            <div class="guide__list-bullet"><img src="./media/images/tick-solid.png"></div>
            <div class="guide__list-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit.</div>
          </li>
          <li class="guide__list-item">
            <div class="guide__list-bullet"><img src="./media/images/tick-solid.png"></div>
            <div class="guide__list-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit. Lorem ipsum dolor sit amet consectetur, adipisicing elit. Vitae doloribus eius ipsum neque, magni labore quasi deleniti a, fugit cum atque saepe delectus illo quis voluptas cumque ratione ut odit.</div>
          </li>
        </ul>
      </div>
      <div class="col-md-4">
        <div class="guide__cta guide__cta--sticky">
          <h3 class="guide__sidebar-header">Ready to Invest?</h3>
          <a href="#" class="guide__btn">Start Trading</a>
        </div>
      </div>
    </div>
  </div>
</section>