<section class="header">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="header__content">
          <h1 class="header__title">The title of the page goes here</h1>
          <h2 class="header__sub-title">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit dolor sit amet, consectetur.</h2>
          <ul class="header__features">
            <li class="header__feature">
              <img src="./media/images/mobile-icon.png" class="header__feature-icon">
              <div class="header__feature-tag">A feature</div>
            </li>
            <li class="header__feature">
              <img src="./media/images/mobile-icon.png" class="header__feature-icon">
              <div class="header__feature-tag">A feature</div>
            </li>
            <li class="header__feature">
              <img src="./media/images/mobile-icon.png" class="header__feature-icon">
              <div class="header__feature-tag">A feature</div>
            </li>
            <li class="header__feature">
              <img src="./media/images/mobile-icon.png" class="header__feature-icon">
              <div class="header__feature-tag">A feature</div>
            </li>
          </ul>
        </div>
      </div>
      <div class="header__ratings-container col-lg-4">
        <div class="header__ratings">
          <div class="header__ratings-wrap">
            <img src="./media/images/thumbsup.png" class="header__ratings-icon">
            <div>
              <div class="header__ratings-header">Top Broker User Ratings</div>
              <div class="header__ratings-rating">
                <div class="header__stars">
                  <img src="./media/images/star.png">
                  <img src="./media/images/star.png">
                  <img src="./media/images/star.png">
                  <img src="./media/images/star.png">
                  <img src="./media/images/star.png">
                </div>
                <div class="header__rating-figure">4.8</div>
              </div>
              <div class="header__ratings-reviews">5000 Reviews</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>