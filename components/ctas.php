<section class="ctas">
  <div class="container">
    <h2 class="ctas__header">We search over <span class="ctas__header--highlighted"> 120 Cryptocurrency Brokers.</span> Many have exclusive trading deals.</h2>
    <div class="ctas__items">
      <div class="ctas__item">
        <img src="./media/images/plus500.png" alt="" class="ctas__brand">
        <a href="#" class="ctas__button">Trade Now</a>
      </div>
      <div class="ctas__item">
        <img src="./media/images/plus500.png" alt="" class="ctas__brand">
        <a href="#" class="ctas__button">Trade Now</a>
      </div>
      <div class="ctas__item">
        <img src="./media/images/plus500.png" alt="" class="ctas__brand">
        <a href="#" class="ctas__button">Trade Now</a>
      </div>
      <div class="ctas__item">
        <img src="./media/images/plus500.png" alt="" class="ctas__brand">
        <a href="#" class="ctas__button">Trade Now</a>
      </div>
    </div>
  </div>
</section>