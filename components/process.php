<section class="process">
  <div class="container">
    <h2 class="process__header">Lorem ipsum consectetur adipisicing <span class="process__header--highlighted">elit dolor</span> sit amet, consectetur adipisicing elit.</h2>
    <div class="row">
      <div class="col-lg-5">
        <div class="process__sub-header"><strong>Xumhic</strong> quae doloremque perferendis est, sit quos esse iste. Optio itaque incidunt sint. Inventore, quod.</div>
        <a href="#" class="process__start-btn process__start-btn--lg">Start Now</a>
      </div>
      <div class="col-lg-7">
        <div class="process__ctgs-container">
          <div class="process__ctgs">
            <div class="process__ctgs-header">Sit amet consectetur adipisicing elit. Accusantium deserun </div>
            <ul class="process__ctgs-items">
              <li class="process__ctg">
                <img src="./media/images/education.png" class="process__ctg-icon">
                <div class="process__ctg-tag">Education</div>
              </li>
              <li class="process__ctg">
                <img src="./media/images/education.png" class="process__ctg-icon">
                <div class="process__ctg-tag">Education</div>
              </li>
              <li class="process__ctg">
                <img src="./media/images/education.png" class="process__ctg-icon">
                <div class="process__ctg-tag">Education</div>
              </li>
              <li class="process__ctg">
                <img src="./media/images/education.png" class="process__ctg-icon">
                <div class="process__ctg-tag">Education</div>
              </li>
              <li class="process__ctg">
                <img src="./media/images/education.png" class="process__ctg-icon">
                <div class="process__ctg-tag">Education</div>
              </li>
              <li class="process__ctg">
                <img src="./media/images/education.png" class="process__ctg-icon">
                <div class="process__ctg-tag">Education</div>
              </li>
            </ul>
          </div>
          <a href="#" class="process__start-btn process__start-btn--sm">Start Now</a>
        </div>
      </div>
    </div>
  </div>
</section>