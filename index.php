<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.min.css">
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="./dist/global.css">
  <title>Broker Options</title>
</head>

<body>
  <?php include './components/banner.php' ?>
  <?php include './components/header.php' ?>
  <?php include './components/tableph.php' ?>
  <?php include './components/payments.php' ?>
  <?php include './components/process.php' ?>
  <?php include './components/topCoins.php' ?>
  <?php include './components/ctas.php' ?>
  <?php include './components/partners.php' ?>
  <?php include './components/guide.php' ?>
  <?php include './components/convinced.php' ?>
  <?php include './components/footer.php' ?>
  <script src="./dist/global.js"></script>
</body>

</html>