const toggleTexts = document.querySelectorAll("[data-toggle-text]");
const toggleButtons = document.querySelectorAll("[data-toggle-button]");

class Toggle {
  switchClass(id, classAdd, classRemove) {
    let add = id.classList.add(classAdd);
    let remove = id.classList.remove(classRemove);
    return { add, remove };
  }

  showMoreLess(text) {
    if (text.classList.contains("partner__toggle--inactive")) {
      this.switchClass(
        text,
        "partner__toggle--active",
        "partner__toggle--inactive"
      );
    } else {
      this.switchClass(
        text,
        "partner__toggle--inactive",
        "partner__toggle--active"
      );
    }
  }

  changeButtonText(button) {
    if (button.innerHTML === "Show More") {
      button.innerHTML = "Show Less";
    } else {
      button.innerHTML = "Show More";
    }
  }
}

const toggle = new Toggle();

toggleButtons.forEach((button, i) => {
  button.addEventListener("click", () => {
    toggle.showMoreLess(toggleTexts[i]);
    toggle.changeButtonText(button);
  });
});
